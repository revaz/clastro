#!/usr/bin/env python3

import Ptools as pt
import numpy as np

n = 100

a = np.linspace(1e-3,1,n)

atrans = 0.2631

ecom  = np.ones(n)
ephys = ecom * atrans/a
emaxphys = ecom *atrans


etrue = np.where(ephys<ecom,ephys,ecom)

pt.plot(a,ecom*a,c="k",ls='--',label=r'$\epsilon_{\rm{com}}\cdot a$')
pt.plot(a,emaxphys,c="b",label=r'$\epsilon_{\rm{phys}}^{\rm{max}}$')

pt.plot(a,etrue*a,c="r")

pt.axis((0,1,0,2))

pt.xlabel(r"$a$")
pt.ylabel(r"$\epsilon\cdot a$")

pt.legend()

#pt.show()
pt.savefig("softening_phys.png")





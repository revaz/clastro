#!/usr/bin/env python3

import Ptools as pt
import numpy as np

n = 100

a = np.linspace(1e-3,1,n)

atrans = 0.2631

ecom  = np.ones(n)
ephys = ecom * atrans/a


etrue = np.where(ephys<ecom,ephys,ecom)

pt.plot(a,ecom,c="k",ls='--',label=r'$\epsilon_{\rm{com}}$')
pt.plot(a,ephys,c="b",label=r'$\epsilon_{\rm{phys}}^{\rm{max}}/a$')

pt.plot(a,etrue,c="r")

pt.axis((0,1,0,2))

pt.xlabel(r"$a$")
pt.ylabel(r"$\epsilon$")

pt.legend()

#pt.show()
pt.savefig("softening_como.png")





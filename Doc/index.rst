.. clastro documentation master file, created by
   sphinx-quickstart on Mon Sep  3 16:08:47 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to clastro's documentation!
===================================

Clastro stands for Computing @LASTRO. It offers a set of resources and information 
to help students to use tools to run and analyses N-body simulations of galaxies or of portions
of the Universe.


Getting started
---------------

Minimum technical knowledge
...........................

Here is a list of technical subjects the student should be at ease with before starting.
A set of links and tutorials is provided to help students to be up to date with the different tools and 
technique required.

.. toctree::
   :maxdepth: 2
      
   rst/ITMinimum
   
   
Theoretical resources
.....................

The following link gives a non-exhaustive list of important papers in different fields 
of galaxy formation.


.. toctree::
   :maxdepth: 2
      
   rst/Litterature
   


Clastro in practice
....................

The following pages introduce how to use the clastro tools on the servers of the Geneva Observatory

.. toctree::
   :maxdepth: 1

   rst/Connecting
   rst/Loading
   rst/Computing




How-to guides
-------------

Step-by-step guides covering common questions and problems for your simulation or data analysis.

.. toctree::
   :maxdepth: 1

   rst/Howto/RunAFullCosmologicalSimulation
   rst/Howto/SetParametersForCosmologicalInitialConditions
   rst/Howto/GenerateCosmologicalZoominSimulationsWithMUSIC
   rst/Howto/ConvertGadgetFilesFromMUSICToSwift
   rst/Howto/InstallClastro


Ressources
----------

The following pages provide links to the common specialized tools the student will have to deal with.

The core programs of Clastro group are Swift and pNbody. Swift's documentation (LINK) is well crafted and you should always have a look at it when using it. 
pNbody's documentation is under construction and this quickstart guide (LINK) and the related pages (LINK) provide some useful tricks to master it.


.. toctree::
   :maxdepth: 2
	 
   rst/GalaxyEvolutionCodes   
   rst/CosmologicalICs.rst
   rst/PythonPackages






Tools and Python packages
=========================

The following python packages represent the core of the ``clastro`` reduction tools.

* `pNbody  : the python Nbody toolbox                                                 <https://obswww.unige.ch/~revaz/pNbody>`_
* `Gtools  : additional galaxy utilities                                              <https://obswww.unige.ch/~revaz/Gtools>`_
* `Ptools  : plotting utilities                                                       <https://obswww.unige.ch/~revaz/Ptools>`_
* `Mtools  : movies utilities                                                         <https://obswww.unige.ch/~revaz/Mtools>`_
* `glups   : an Nbody stereo viewer                                                   <https://obswww.unige.ch/~revaz/glups>`_
* `PyRates : a Python module for radiative transfer rates from custom UV spectra      <https://obswww.unige.ch/~revaz/PyRates>`_
* `PyGrackle : a python wrapper to the GEAR cooling Grackle library                   <https://obswww.unige.ch/~revaz/PyGrackle>`_
* `PyChem    : python wrapper to the GEAR chemistry module                            <https://obswww.unige.ch/~revaz/PyChem>`_ 


Specific chemical tables are obtained through `PyChemTables <https://obswww.unige.ch/~revaz/PyChemTables>`_.

Python 3 Tutorials
******************

.. toctree::
   :maxdepth: 2
   

   python3/python_short
   python3/python
   ./Notebook/Numpy3.ipynb
   ./Notebook/matplotlib3.ipynb
   ./Notebook/scipy3.ipynb
   ./Notebook/pNbody3.ipynb

Notebooks
---------

1. :download:`Numpy Notebook <./Notebook/Numpy3.ipynb>`
2. :download:`Matplotlib Notebook <./Notebook/matplotlib3.ipynb>`
3. :download:`Scipy Notebook <./Notebook/scipy3.ipynb>`
4. :download:`pNbody Notebook <./Notebook/pNbody3.ipynb>`

   



   
   

   

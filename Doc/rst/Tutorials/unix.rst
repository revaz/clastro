.. _unix-tutorial:

Unix Tutorial
*************

Reference to the wiki : `<https://obswww.unige.ch/obswiki/Lectures/UnixIntroduction>`_.



Avant de commencer ce tutoriel
##############################

::

  cp -r /unige/tutoriel .
  chmod u+rw tutoriel
  cd tutoriel


Système de fichiers
###################


* fichiers:
    ``/usr/share/man/man1/rm.1.gz``

* répertoires (dossier):
    ``/home``


Commandes, shell et variables d'environnement
#############################################

* https://obswww.unige.ch/obswiki/UnixCommand/


le shell
========

* dialogue entre l'utilisateur et le système
* interprète des commandes tapées par l'utilisateur
* en général ::
    
    commande + options + fichiers/repertoire/texte
    
* exemple ::

    ls -lrt Readme


différents types de shell
-------------------------

* quelques shells::
  
   /bin/bash
   /bin/sh
   /bin/csh
   /bin/tcsh

* shell par defaut::
 
    echo $SHELL
 


configuration
-------------

* bash :: 

    $HOME/.bashrc

* tcsh :: 

    $HOME/.tcshrc



variables d'environnement
-------------------------

Variables qui permettent d'influencer le comportement du shell.

* list des variables définies::
 
    env


* variables importantes::
 
    echo $HOME
    echo $PATH
    echo $LD_LIBRARY_PATH



executer une commande
=====================

Example::

  nedit

le shell reconnait une commande, car elle est
contenue dans des répertoires particuliers
dont la liste est dans la variable ``PATH``.


existence et chemin d'une commande
==================================

* which::

    which gcc
    which commande_inexistante


modules d'extension
===================

* utilisation de logiciels/librarires spécifiques, non standard::
 
    topcat
    which topcat

    module ava
    module list
    module add topcat/3.6
    module list

    echo $PATH

    which topcat
    topcat

    module remove topcat



Aide à l'entrée de commandes
############################
 

Simplification de l'entrée des commandes
========================================

* ``TAB`` :  auto-completion/propositions
* ``FLECHE HAUT`` : retour aux commandes précédentes
* liste des précédentes commandes ::
  
    history
 

aide en ligne
=============

::

 man who
 who -h
 who --help





Les commandes importantes 
#########################



attributs personnels
====================

* username::

    whoami

* identificateurs::

    id

* home (espace de travail privé) ::

    echo $HOME


mot de passe
============
 
* changement du mot de passe (! mot de passe exigent)::
 
    passwd
 



informations sur les fichiers
=============================

* type de fichier::

    file /bin/ls
    file binary.dat
    file Readme


* affichier le contenu d'un fichier::

    cat Readme
    head -n 1 Readme
    tail -n 1 Readme

    more Readme
    less Readme

 



substitution de caratères
=========================

* ``*`` : zéro ou plusieurs caractères
* ``?`` : un seul caractère
* ``[]``: selection de caractères
* examples ::
 
    touch Reaame
    touch Reazme
    touch Reaqme
    touch ReaDme
    touch Rea\$me
    touch Readdme

    ls *
    ls Rea*
    ls Rea?me
    ls Rea[a-z]me
    ls Rea[dD]me
    ls Rea[^a-z]me

 


fichiers et répertoire
======================

fichiers
--------

* création d'un fichier vide::
 
    touch fichier_vide.txt
    ls
  

* edition::
 
    nedit fichier_vide.txt &
    gedit fichier_vide.txt &
    kate fichier_vide.txt &
    emacs fichier_vide.txt &
    xemacs fichier_vide.txt &

    module add jedit
    jedit fichier_vide.txt &

    vi fichier_vide.txt
    vim fichier_vide.txt
 


répertoires
-----------


* création d'un répertoire::
 
    mkdir pgm
 
 
* déplacement dans le répertoire crée::
 
    cd pgm
 
* affichage de la position actuel dans le système de fichier::
 
    pwd
 
* création d'un répertoire et sous-répertoire::
 
    mkdir -p mydir/subdir
 


racourcis de chemin
-------------------

 * ``.`` 	répertoire courant
 * ``/`` 	répertoire mère
 * ``../``	répertoire précédent
 * ``~``        répertoire home
 
exemples::
 
  cd ../
  pwd
  cd ~
  pwd
  cd tutoriel



copier
------


 * copier un fichier::

     cp fichier.txt pgm/.


 * copier un répertoire et son contenu::

     cp -r pgm pgm2



 * copier en conservant tous les attributs::

     rsync -av --progress pgm/ pgm.bkp 


suppression
-----------

 * supprimer un fichier:: 
 
     rm pgm2/fichier.txt
 

 * supprimer un répertoire::
 
     rm -rf pgm2
 
 
 * supprimer prudemment::

     rmdir pgm.bkp
     ls pgm.bkp
     rm -rf pgm.bkp/*
     rmdir pgm.bkp


lien
----

 * créer un lien::
 
     ln -s pgm/fichier.txt lien.txt
     ls -la
     ls -lrt



modification des attributs des fichiers
---------------------------------------

 * date de dernière modification::
 
     ls -la pgm/fichier.txt 
     touch pgm/fichier.txt 
     ls -la pgm/fichier.txt 


 * permissions::
 
     chmod go-rw pgm/fichier.txt
     ls -la pgm/fichier.txt 



autres informations sur les fichiers
------------------------------------

 * liste des lignes, mots, caratères::

     wc Readme
 
 
 * tri des lignes::
 
     sort Readme
     
     cat numbers.txt
     sort numbers.txt 



comparaison de fichier/répertoires
----------------------------------

 * differences entre deux fichiers::
 
     diff pgm/fichier.txt pgm.orig/fichier.txt
     meld pgm/fichier.txt pgm.orig/fichier.txt
 

 * differences entre deux répertoires::
 
     diff pgm pgm.orig
     meld pgm pgm.orig


recherche
=========

chercher dans un fichier 
------------------------
 
 * egrep::
 
     egrep ftp pgm/fichier.txt 
     egrep -v ftp pgm/fichier.txt 
     egrep  FTP pgm/fichier.txt 
     egrep -i FTP pgm/fichier.txt 
 

chercher un fichier
-------------------

 * find::
 
     find . -name fichier.txt
     find . -name 'fich*'
 

 * locate::
 
     locate qq
     locate -c qq

     locate ssqquote
     locate -b ssqquote



espace disque et taille de fichiers
===================================

 * liste des disques montés::
 
     df -h
 
 * liste des disque locaux montés::

     df -hl
 

 * taille de fichiers, répertoires::
 
     du -csh *




date, heure, temps
==================


 * afficher l'heure et la date::

     date
 

 * calendrier::
 
     cal
     cal 2013
 
 
 * temps nécessaire à l'execution::

     time sleep 1 
     # real time / user cpu time /system cpu time
 
 * pause::
 
     sleep 1


Outils divers
#############

Web et messagerie 
=================================

 * web:
 
   * firefox
   * konqueror
   
  
 * mail:
  
   * thunderbird
   * kmail
  
Editeurs de texte simples
=========================
 
 * vi
 * vim
 * emacs
 * xemacs
 * nedit
 * gedit
 * jedit
 * kate
 * geany
 * bluefish

Langages interprétés
====================

 * python2, python3
 * ruby
 * julia
 * perl


Compilateurs
============

 * C/C++
 
   * gcc
   * g++
   * icc
   
 * fortran  
 
   * f77
   * gfortran
   * ifort 
 

Outils de visualisation/d'édition 
=================================

 * texte:

   * ooffice
   * latex
   * kile

 * ps/eps:
 
   * gv
   * okular
   * evince

 * pdf:
  
   * okular
   * evince

 * images:
  
   * display
   * gimp


Dessin
=================================

 * gimp 
 * ooffice
 * inkscape
 * FreeCAD (3d)
 * LibreCAD (2d)



Jobs et processus
#################


 * affichage de tous les processus:: 
 
     ps -ef
 
 * affichage des processus appartenant à revaz::
 
     ps -fu revaz
 
 
 * affichage dynamique::
 
     top
 
 * background/forground::
 
     xclock -bg blue
     <Ctrl>+z  bg
     jobs
     fg 1
     <Ctrl>+c 
 
 * envoyer directement en background::
 
     xclock &

 * tuer un processus::
 
    jobs -l
    kill -15 JOBID

 * tuer un processus interactivement::
 
     xclock &
     xkill

 * priorité::
 
     nice -19 xclock &
     xclock &
     renice 19 JOBID


Archivage
#########

 * créer une archive avec ``tar``::

     mkdir archive
     touch archive/file1
     touch archive/file2
     touch archive/file3

     tar -cf archive.tar archive
     tar -tf archive.tar

     tar -czf archive.tar.gz archive
     tar -ttf archive.tar.gz
     tar -xzf archive.tar.gz


 * compresser/décompresser

   * gzip/gunzip::
   
       gzip archive.tar archive.tar.gz
       gzip -1 archive.tar archive.tar.gz
       gzip -9 archive.tar archive.tar.gz
       gunzip archive.tar.gz
   
   * bzip2/bunzip2::
   
       bzip2/bunzip2
       bzip2 archive.tar
       bunzip2 archive.tar.bz2
  
  * zip/unzip::
  
      zip/unzip
      zip archive.tar.Z archive.tar
      unzip archive.tar.Z archive.tar



Programmation et langages de programation
#########################################
 
 * bash
 * python
 * perl
 * fortran
 * gcc
 * g++


Environnement de développement
==============================

 * eclipse

Compilation
===========

 * C::

     cd Make
     gcc main.o
     ./a.out

Makefile
========

  ::

     make

     make clean
     touch main.c
     make
     touch main.o
     make
     make -n
     make -f Makefile-v2


Librairies
==========
 
 * .so : dynamique
 * .a  : statique
 
 ::



   lsof /lib64/libc.so.6
   which bash
   ldd /bin/bash

   # --> exemple sqrt (math.)
   $LD_LIBRARY_PATH


programmer un shell
===================

Shell : dialogue entre l'utilisateur et le systèm

* /bin/bash
* /bin/sh
* /bin/csh
* /bin/tcsh

shell utilisé::

  echo $SHELL


bash
====


variables
---------

::

  env


structure lexicale
------------------

* caractères spéciaux::

    & | ; < > ( )

    || && |& >& 
      
    #

* redirection des entrées sorties

  * | > >>

  exemples::

    echo $HOME

    echo $HOME > myHome.txt
    cat myHome.txt | grep revaz
    echo $HOME >> myHome.txt

    ls qq 
    ls qq > qq.txt
    ls qq >& qq.txt

    cat < myHome.txt




substitution de commandes
-------------------------

::

  echo le repertoire contiend :  `ls`


substitution historiques
------------------------


* history

  * !!	        : dernière commande
  * !-10	: nième dernière commande


alias
-----

::

  alias ls='ls -lrt'
  unalias ls


boucles
-------

::
  
  for num in 1 2 3 ; do echo $num ; done
  for file in * ; do echo $file ; done
  for i in {0..10..2}; do echo $i; done


tests
-----

::

  if [ -r Readme ]
  then 
    echo "le fichier est present"
  else
    echo "le fichier est absent"
  fi  
    

affectation de variable
-----------------------

::

  a=1
  b="du texte"
  echo $a $b
  unset a


création d'un script
--------------------

  
* entête : `#!/usr/bin/env bash`
* permission ::
  
    chmod a+x





Impression 
==========

Impression en commande en ligne
-------------------------------

  ::

    lpr -Php0 file1
    lpq -Php0
    lprm  -Php0
    lpstat -Php0


.. _litterature:

Litterature
###########


Galaxy Evolution Codes
----------------------

 * `SWIFT: A modern highly-parallel gravity and smoothed particle hydrodynamics solver for astrophysical and cosmological applications <https://ui.adsabs.harvard.edu/abs/2024MNRAS.530.2378S/abstract/>`_ 
 * `The cosmological simulation code GADGET-2  <https://ui.adsabs.harvard.edu/abs/2005MNRAS.364.1105S/abstract/>`_ 
 * `E pur si muove: Galilean-invariant cosmological hydrodynamical simulations on a moving mesh <https://ui.adsabs.harvard.edu/abs/2010MNRAS.401..791S/abstract/>`_
 * `A new class of accurate, mesh-free hydrodynamic simulation methods <https://ui.adsabs.harvard.edu/abs/2015MNRAS.450...53H/abstract/>`_

Smooth Particles Hydrodynamics (SPH)
------------------------------------

 * `Smoothed Particle Hydrodynamics <https://ui.adsabs.harvard.edu/abs/2005PhDT.......295P/abstract/>`_ 
 * `Smoothed Particle Hydrodynamics and Magnetohydrodynamics <https://ui.adsabs.harvard.edu/abs/2012JCoPh.231..759P/abstract/>`_ 
 * `Smoothed Particle Hydrodynamics in Astrophysics <https://ui.adsabs.harvard.edu/abs/2010ARA%26A..48..391S/abstract/>`_ 
 * `A General Class of Lagrangian Smoothed Particle Hydrodynamics Methods and Implications for Fluid Mixing Problems <https://ui.adsabs.harvard.edu/abs/2013MNRAS.428.2840H/abstract/>`_ 
 * `SPHENIX: smoothed particle hydrodynamics for the next generation of galaxy formation simulations <https://ui.adsabs.harvard.edu/abs/2022MNRAS.511.2367B/abstract/>`_
 * `The Lagrangian hydrodynamics code MAGMA2 <https://ui.adsabs.harvard.edu/abs/2020MNRAS.498.4230R/abstract/>`_

Gravitational Softenning
------------------------
 
 * `Towards optimal softening in three-dimensional N-body codes - I. Minimizing the force error <https://ui.adsabs.harvard.edu/abs/2001MNRAS.324..273D/abstract/>`_
 * `Optimal Choice of the Softening Length and Time Step in N-body Simulation <https://ui.adsabs.harvard.edu/abs/2005ARep...49..470R/abstract/>`_
 * `Novel conservative methods for adaptive force softening in collisionless and multispecies N-body simulations <https://ui.adsabs.harvard.edu/abs/2023MNRAS.525.5951H/abstract/>`_
 

Cooling/Heating
---------------

 * `Cosmological Simulations with TreeSPH <https://ui.adsabs.harvard.edu/abs/1996ApJS..105...19K/abstract/>`_
 * `Grackle: Chemistry and radiative cooling library for astrophysical simulations <https://ui.adsabs.harvard.edu/abs/2016ascl.soft12020S/abstract/>`_

Chemical evolution
------------------

 * `Computational issues in chemo-dynamical modelling of the formation and evolution of galaxies  <https://ui.adsabs.harvard.edu/abs/2016A%26A...588A..21R/abstract/>`_


Feedback processes
------------------

 * `FIRE-2 simulations: physics versus numerics in galaxy formation <https://ui.adsabs.harvard.edu/abs/2018MNRAS.480..800H/abstract/>`_
 * `FIRE-3: updated stellar evolution models, yields, and microphysics and fitting functions for applications in galaxy simulations  <https://ui.adsabs.harvard.edu/abs/2023MNRAS.519.3154H/abstract/>`_

Radiative Rransfer
------------------

 * `GEAR-RT: Towards Exa-Scale Moment Based Radiative Transfer For Cosmological Simulations Using Task-Based Parallelism And Dynamic Sub-Cycling with SWIFT   <https://ui.adsabs.harvard.edu/abs/2023PhDT.........3I/abstract/>`_



Dwarf Galaxies and ultra-faint dwarf galaxies (UFDs)
----------------------------------------------------

 * `The Observed Properties of Dwarf Galaxies in and around the Local Group  <https://ui.adsabs.harvard.edu/abs/2012AJ....144....4M/abstract/>`_
 * `The Faintest Dwarf Galaxies  <https://ui.adsabs.harvard.edu/abs/2019ARA%26A..57..375S/abstract/>`_
 * `The dynamical and chemical evolution of dwarf spheroidal galaxies with GEAR <https://ui.adsabs.harvard.edu/abs/2012A%26A...538A..82R/abstract/>`_
 * `Pushing back the limits: detailed properties of dwarf galaxies in a ΛCDM universe <https://ui.adsabs.harvard.edu/abs/2018A%26A...616A..96R/abstract/>`_
 * `How much metal did the first stars provide to the ultra-faint dwarfs? <https://ui.adsabs.harvard.edu/abs/2023A%26A...669A..94S/abstract/>`_






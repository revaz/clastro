MUSIC
=====

* `MUSIC <https://www-n.oca.eu/ohahn/MUSIC>`_.




To generate the initial conditions (ICs) for cosmological simulations, a common tool is MUSIC. Its user guide serve as documentation. Refer to this


The first thing to do, is to create the parameter file. Here is an example:  




Notice that we use the Planck XXXX cosmology and the parameters were set accordingly. Those parameters need to be set with the exact same values in Swift, otherwise it will complain and the simulation will not start. 

If you want to generate a zoom-in simulation, you need to provide a file containing the position of the particles. Here is an example of such file XXX. 


Important note: MUSIC does not output in Swift format natively. Choses Gadget format and continue reading to learn how to transform Gadget format file to Swift format. 


Once you when you parameter file, you can launch music with sbatch (see documentation here). Choose carefully the number of nodes, cores and the memory allocated. If you do not give enough memory, it will fail (notice that giving more . Then, you wait until it finishes… 


Now, you need to convert the Gadget output file to Swift. This is done by using the script “gad2swift” available in Gtools. You may want to modify it a bit, for example to convert the boundary dark matter particles to the right Swift particle type (type X). You need to change the following line: 


Then, run the script:


Is it finished? No… you need to convert the output to initial conditions… 


Note: There are few differences between Swift initial conditions and Swift output files. The main ones are that some fields are written in singular in the ICs but in plural in the output e.g. : SmoothingLength —> SmoothingLengths. 


To proceed to the conversion to ICs files, use the script “sw_snap2IC” as follow:


Congratulations, you have generated you ICs ! Now, you are ready to prepare Swift parameter file ! A few notes about it:

    Gadget file have their units in /h. The conversions we made do not remove those conversions. Swift ICs need to have the units without the /h. Wonderfully, Swift cams do the cleaning for us ! Just put the following lines in you Swift parameter file: 
    As stated above, in the Cosmology section, you have to put the exact same values of the cosmological parameters than in your MUSIC configuration file. If not, Swift may complain because it will ensure that the values in the parameter files agree with the content ont your ICs (which were generated according to some cosmological parameters). 


Notes:
------

 Il faut mettre à jour puisque maintenant on n’a besoin que d’un seul fichier pour convertir du format gadget à Swift. Les exemples de fichiers sont dans /projects/astro/clastro/roduit/sinks/UFDs/zoom. Un bout de readme se trouve dans /projects/astro/clastro/roduit/sinks/UFDs/new_run/README.org. Le readme dans le dossier zoom a été effacé sur jed par leur nouveau système de nettoyage automatique. :/ 



.. _gear:

GEAR
####

``GEAR`` is a Chemo-dynamical Tree/SPH code dedicated to the study of formation and evolution of galactic 
structures.

Reference ``GEAR`` simulations may be found here::

  /projects/astro/clastro/gear/ref-sim 
  


Web page of GEAR
================

The Gear webpage is `here <https://obswww.unige.ch/~revaz/Gear>`_ .

Cloning GEAR
=============

* clone::
   
   git clone https://gitlab.com/revaz/gear.git


Modules
=======

* On lesta load the following modules::
     
     module use /opt/ebmodules/all/Core
     module use /projects/astro/clastro/local/lesta2023/soft/modulefiles/
     
     module purge
     module add clastro/dev
     module add gear

* On Yggdrasil (login1.yggdrasil.hpc.unige.ch), you will need the following modules and to compile your own version of grackle::
     

     module use /home/revazy/local/soft/modulefiles/
     
     module purge
     module add clastro/dev
     module add gear


Compiling GEAR
===============

* To compile it from the source directory ``src`` use::
   
    rm -rf CMakeCache.txt CMakeFiles
    cmake . -DOPTS=build_options/COSMO_FULL_CHEMISTRY_GRACKLE3_FFTW.txt
    make -j 8

Examples of GEAR runs
=====================

on lesta::

  /projects/astro/clastro/gear/examples








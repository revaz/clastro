Connecting to lesta cluster
---------------------------

``lesta`` is the cluster hosted at the Observatoire de Genève.
To get an access to it, send a mail to `here <mailto:astro-it-support@unige.ch>`_.

To get more information on how to use it and how to log into it, 
see
`the wiki pages of the Computing System of the Geneva Observatory <https://www.astro.unige.ch/wiki/IT/doc/>`_.




Computing
=========

Computing on LESTA
..................

To compute on the ``lesta`` cluster, use the 
`Slurm Workload Manager <https://slurm.schedmd.com/documentation.html>`_.
The document of `slurm` documentation specific to the LESTA cluster is described 
`on the following wiki page <https://www.astro.unige.ch/wiki/IT/doc/astroge/lesta#Computing_on_Lesta>`_.


Computing on BONSAI
...................

To compute on the ``bonsai`` cluster, use the 
`Slurm Workload Manager <https://slurm.schedmd.com/documentation.html>`_.
The document of `slurm` documentation specific to the LESTA cluster is described 
`on the following bonsai wiki page <https://www.astro.unige.ch/wiki/IT/doc/astroge/bonsai>`_.




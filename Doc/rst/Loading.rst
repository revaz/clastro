Loading Clastro on lesta
------------------------

Load modules
~~~~~~~~~~~~

on lesta::

  module use /opt/ebmodules/all/Core
  module use /projects/astro/clastro/local/lesta2023/soft/modulefiles/
  
  module purge
  module add clastro/dev

If you need the most up to date version of `pNbody <https://lastro.epfl.ch/projects/pNbody>`_ 
(however, not necessarily the most stable one), use::

  module add clastro/devy

instead.


How to test ?
~~~~~~~~~~~~~

Try the following commands::

 pNbody_test
 grackle3_test
 pychem_test


Configure 
~~~~~~~~~

If you expect to work with the MIST database, you will need
to define additional environment variables that point toward
the database you want to use. One example of this database is available on 
``lesta`` and you need to define ::

  export PNBODY_MIST_DEFAULT_DATABASE=/projects/astro/clastro/data/MIST/MIST_v1.2_vvcrit0.0_SDSSugriz
  export PNBODY_MIST_DEFAULT_FILTER_SYSTEM=SDSS

in order to use it.



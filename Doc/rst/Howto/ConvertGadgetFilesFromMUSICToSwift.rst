How to convert gadget files from MUSIC output to Swift initial conditions
-------------------------------------------------------------------------

First, check that the configuration file of ``MUSIC`` contains the following lines::

  [output]
  format                  = gadget2
  gadget_usekpc           = yes
  filename                = snap.dat

The convertion can then be performed with the command ``sw_gad2swift`` from ``pNbody``

Example::

  sw_gad2swift snap.dat -o snap.new.hdf5 --disable_interactive_mode
  
or, if you prefere to check the units, and the box size::
  
  sw_gad2swift snap.dat -o snap.new.hdf5
  

.. note::
  
  If other units are used, you will have to enter the interactive mode of ``sw_gad2swift`` (which is the default)
  and give others values manually.
  
  
.. note::

  The example above will work with the following units in the ``Swift`` parameter file::
  
    InternalUnitSystem:
      UnitMass_in_cgs:     1.98841e43    # 10^10 M_sun in grams
      UnitLength_in_cgs:   3.085678e21 # kpc in centimeters
      UnitVelocity_in_cgs: 1e5           # km/s in centimeters per second
      UnitCurrent_in_cgs:  1             # Amperes
      UnitTemp_in_cgs:     1             # Kelvin
  
  as well as with the initial conditions parameters::
  
    InitialConditions:
      cleanup_h_factors:           1
      cleanup_velocity_factors:    1
  
  
  
  






.. _installation:

Install Clastro
###############



Install the clastro python package
----------------------------------


The easiest is to install `clastro` via `pip`::

  pip install git+https://gitlab.com/revaz/clastro.git

or::
   
  pip install git+https://gitlab.com/revaz/clastro.git --prefix path_to_the_directory_you_want

or, clone and install ::

  git clone https://gitlab.com/revaz/clastro.git
  pip install clastro
  


Configure a module 
------------------

Need to add the following variables::


  setenv("CLASTRO_PYTHON",              "/astro/soft/common/gcc/5.4.0/python/3.7.1/bin/python3")    -- path to the python executable
  setenv("CLASTRO_PYTHON_INSTALL_PATH", "/astro/soft/clastro/v2/python/3.7/" )                      -- path to the module install directory
  setenv("CLASTRO_DIRECTORY",           "/astro/soft/clastro/v2/python/3.7/clastro" )               -- path to the git repo





Install other packages
----------------------

Directly from the git with pip
::

  pip install git+https://gitlab.com/revaz/pNbody.git
  pip install git+https://gitlab.com/revaz/Gtools.git
  pip install git+https://gitlab.com/revaz/Ptools.git
  pip install git+https://gitlab.com/revaz/Mtools.git
  pip install git+https://gitlab.com/revaz/Otools.git
  pip install git+https://gitlab.com/clastro/Utools.git


or from the clastro tools:


Clone all::

  clastro_clone_all


or clone individually::

  clastro_clone_pNbody
  clastro_clone_Gtools
  clastro_clone_Mtools
  clastro_clone_Ptools
  clastro_clone_Otools
  clastro_clone_Utools
  clastro_clone_glups
  clastro_clone_PyChem
  clastro_clone_PyGrackle-3.0
  clastro_clone_pyrates


Install or update packages::

  clastro_install_pNbody
  clastro_install_Gtools
  clastro_install_Ptools
  clastro_install_Mtools
  clastro_install_Otools
  clastro_install_Utools  
  clastro_install_glups
  clastro_install_PyChem
  clastro_install_PyGrackle-3.0
  clastro_install_pyrates   



Install other python packages
-----------------------------

::
  
  # some python standards
  pip install --ignore-installed --prefix=$CLASTRO_PYTHON_INSTALL_PATH    matplotlib==3.4.3
  pip install --ignore-installed --prefix=$CLASTRO_PYTHON_INSTALL_PATH    qtconsole
  pip install --ignore-installed --prefix=$CLASTRO_PYTHON_INSTALL_PATH    tqdm
  pip install --ignore-installed --prefix=$CLASTRO_PYTHON_INSTALL_PATH    pyopengl
  pip install --ignore-installed --prefix=$CLASTRO_PYTHON_INSTALL_PATH    numpy
  
  
  # clastro
  pip install git+https://gitlab.com/revaz/clastro.git --prefix $CLASTRO_PYTHON_INSTALL_PATH
  pip install ./clastro  --prefix $CLASTRO_PYTHON_INSTALL_PATH
  
  # clone other packages
  clastro_clone_all
  
  # install all
  clastro_install_pNbody
  clastro_install_Gtools 
  clastro_install_Ptools 
  clastro_install_Mtools 
  clastro_install_Otools 
  clastro_install_Utools 
  clastro_install_glups
  
  # PyChem
  cd PyChem/PyChem
  python3 setup.py build
  python3 setup.py install --prefix $CLASTRO_PYTHON_INSTALL_PATH
  cd ../..
  
  # PyGrackle
  cd PyGrackle-3.0/PyGrackle
  python3 setup.py build
  python3 setup.py install --prefix $CLASTRO_PYTHON_INSTALL_PATH
  cd ../..
    

How to test ?
-------------

Try the following commands::

 pNbody_test
 mist_test
 grackle3_test
 pychem_test









Set Parameters For Cosmological Initial Conditions
##################################################


Setting Softening length
========================

(See Power 2003 eq. 15, or Dehnen and Read 2011.)

In cosmological simulations, one sometimes wants to start a simulation with a softening :math:`\epsilon_\rm{com}` 
that is fixed in comoving coordinates (where the physical softening, :math:`\epsilon_\rm{phys} = a\cdot\epsilon_\rm{com}`, 
then grows proportional to the scale factor :math:`a`, but at a certain redshift one wants to freeze the resulting growth 
of the physical softening :math:`\epsilon_\rm{phys}^\rm{max}` at a maximum value. In ``Gear`` these maximum softening lengths 
are specified by the ``Softening*MaxPhys`` parameters. In the actual implementation, the code uses:

.. math::

  \epsilon_\rm{com} = min(\epsilon_\rm{com},\epsilon_\rm{phys}^\rm{max}/a)

as comoving softening. Note that this feature is only enabled for 
``ComovingIntegrationOn=1``, otherwise the ``*MaxPhys`` values are ignored. 



Softening in comobile coordinates
---------------------------------

It is usual to compute the gravitational softening length in comobile coordinates from the simple formula:

.. math::
  
  \epsilon_\rm{com} = \frac{L}{35\,N}

with :math:`L`, the box size, :math:`N`, number of particles along one dimension.
Note the origin of the value 35 is unclear. In the litterature it is more often 25 (see for example EAGLE,MILLENIUM,OWLS). 


Maximal softening in physical coordinates
-----------------------------------------

If one wants the physical softening to be fixed for example from a redshift :math:`2.8 (a=0.2631)` on:

.. math::

  \epsilon_\rm{phys}^\rm{max} =  \frac{L}{35\,N}\cdot0.2631

Note: the value of 2.8 is not clear, on the litterature, we find 2.8(EAGLE),2.9(OWLS),3(APOSTLE), 9(ERIS)... 

Example of softening
--------------------

For a transition redshift :math:`2.8 (a=0.2631)`, the softening in comobile coordinates
will looks like:

.. image:: ../../plots/softening_como.png
  :width: 800 px

Similarly, in physical coordinates:

.. image:: ../../plots/softening_phys.png
  :width: 800 px



Set the box size, resolution and gravitational softening length
===============================================================

In order to set the box size, resolution and gravitational softening length,
the command ``gsetCosmoBox`` will help you. Example::

  gsetCosmoBox --Mmin 1024 --MgastoMstar 4 --lmin 3 --lmax 9

will returns::

  Ztrans      = 2.8
  MgastoMstar = 4

     level                        # part  box size   box mass    part mass    eps DM c    eps DM p star mass
         3     8**3                  512    3441.6 3.56323e+12 6.959438e+09     12.292       3.235  268435456.0
         4    16**3                 4096    3441.6 3.56323e+12 8.699297e+08      6.146       1.617  33554432.0
         5    32**3                32768    3441.6 3.56323e+12 1.087412e+08      3.073       0.809  4194304.0
         6    64**3               262144    3441.6 3.56323e+12 1.359265e+07      1.536       0.404  524288.0
         7   128**3              2097152    3441.6 3.56323e+12 1.699081e+06      0.768       0.202   65536.0
         8   256**3             16777216    3441.6 3.56323e+12 2.123852e+05      0.384       0.101    8192.0
         9   512**3            134217728    3441.6 3.56323e+12 2.654815e+04      0.192       0.051    1024.0


Initial Redshift
================

How to choose the initial redshift in a (zoom-in)cosmological simulation ? 
You can use the script ``ggetZiforCosmoSims``::

  ggetZiforCosmoSims --N2 64 --N1 512 -L 3.442 --param params 
  
which will return::  
  
  N=  512 zinit(max)=125.5 zinit(min)= 62.2
  N=   64 zinit(max)= 81.9 zinit(min)= 40.5

  zinit should be in [ 62.2, 81.9]




``N2`` is the number of particles along one axis for the lowest level, ``N1`` is the same for the highest one.
The code is based on what is proposed by Onorbe et al. 2013 (see also : Knebe 2009) 

.. image:: ../../images/initial_redshift.png
  :width: 800 px

.. _informatics_minimum:

IT Minimum
==========

Here is a list of technical subjects the student should be at ease with before starting. A set of links and tutorials is provided to help students to be up to date with the different tools and technique required.

Unix/Linux
----------

.. toctree::
   :maxdepth: 1

   Tutorials/unix



Programming languages 
---------------------

`python <https://www.python.org>`_
..................................


.. toctree::
   :maxdepth: 1

   Tutorials/python3



`ANSI C <https://en.wikipedia.org/wiki/ANSI_C>`_
................................................



Version control systems
-----------------------

* `git <https://git-scm.com/>`_
* `gitlab <https://about.gitlab.com>`_ 



Best coding practices
---------------------

* :download:`Mladen's talk on Best Conding Practises in SWIFT <../pdf/BestCodingPractices.pdf>`.
* `Scientific Software Development <http://ada10.cosmostat.org/wp-content/uploads/2023/09/farrens.pdf>`_












.. .. toctree::
..    :maxdepth: 1
.. 
..    Tutorials/unix
..    Tutorials/python3

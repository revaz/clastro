
.. _swiftgear:

SWIFT
#####


SWIFT is an open-source cosmological and astrophysical numerical solver designed to run efficiently on modern hardware. 
A comprehensive and extensive set of models for galaxy formation as well as planetary physics are provided alongside 
a large series of examples.


Web page of SWIFT
=================

* `<https://swift.strw.leidenuniv.nl/>`_.


Reference paper
===============

* `SWIFT: A modern highly-parallel gravity and smoothed particle hydrodynamics solver for astrophysical and cosmological applications <https://ui.adsabs.harvard.edu/abs/2024MNRAS.530.2378S/abstract/>`_ 



Cloning SWIFT
=============

* clone::
   
   git clone https://gitlab.cosma.dur.ac.uk/swift/swiftsim.git


Modules
=======

* On lesta load the following modules::

     module use /opt/ebmodules/all/Core
     module use /projects/astro/clastro/local/lesta2023/soft/modulefiles/
     
     module add clastro/dev
     module add swift

* On Yggdrasil (login1.yggdrasil.hpc.unige.ch), you will need the following modules and to compile your own version of grackle::
     
     module use /home/revazy/local/soft/modulefiles/
     
     module add clastro/dev
     module add swift



Examples of SWIFT runs
======================

``SWIFT`` comes with a large set of examples with a detailed README file on how to configure, compile and run examples.
Here we give all details to run two such examples.


Isolated dwarf galaxy (gravity only)
....................................

From the ``swiftsim`` (cloned) directory.

Configure::

  ./configure
  
Compile::

  make -j12  

Move to the example directory::

  cd examples/IsolateddSph/IsolateddSph_cups

Run::

  ./run.sh
  
This last script download some file needed to run the model.
If you want to only run ``SWIFT``, use the last line of the ``run.sh`` script::

  ../../../swift --stars --self-gravity --threads=14 params.yml 2>&1 | tee output.log





Isolated disk (with hydro, cooling, star formation and feedback)
................................................................


From the ``swiftsim`` (cloned) directory.

Configure::

  ./configure --with-chemistry=GEAR_10  --with-cooling=grackle_0 --with-pressure-floor=GEAR  --with-stars=GEAR --with-star-formation=GEAR --with-feedback=GEAR --with-grackle=${GRACKLE_ROOT}

Compile::

  make -j12  

Move to the example directory::

  cd examples/IsolatedGalaxy/IsolatedGalaxy_multi_component/GEAR

Run::

  ./run.sh
  
This last script download some file needed to run the model.
If you want to only run ``SWIFT``, use the last line of the ``run.sh`` script::

  ../../../../swift --hydro --stars --star-formation --self-gravity --feedback --cooling --threads=14 params.yml 2>&1 | tee output.log









SWIFT-GEAR
==========

``SWIFT-GEAR`` is a physical model of the ``SWIFT`` code, a cosmological particle based code.

Some non official GEAR-Swift documentation on sink-particles is `here
<https://obswww.unige.ch/~revaz/gear-swift/>`_.

``CSDS``
========

The ``CSDS`` for Continuous Simulation Data Stream is a new output system for SWIFT 
designed to replace the standards snapshots.

It may be cloned from `this link <https://gitlab.cosma.dur.ac.uk/lhausammann/csds-reader>`_ .
Its documenation is `here too <https://continuous-simulation-data-stream.readthedocs.io/en/latest/QuickStart/index.html>`_ .




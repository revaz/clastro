#!/usr/bin/env python
'''
 @package   clastro
 @file      setup.py
 @brief     Install clastro
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2018 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of clastro.
'''

from setuptools import setup, find_packages
import sys, os
from glob import glob


# scripts to install
scripts = glob("scripts/*")

setup(
    name="clastro",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz/clastro",
    description="""clastro module""",
    license="GPLv3",
    version="5.0",

    packages=find_packages(),
    scripts=scripts,
    
    install_requires=['numpy','scipy','h5py','astropy','Pillow','matplotlib']
)

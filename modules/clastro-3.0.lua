-- -*- lua -*-
-- Module file created by Yves Revaz Tue Aug 14 13:36:42 CEST 2018
--
-- clastro@3.0
--

whatis([[Name : clastro]])
whatis([[Version : 3.0]])
whatis([[Short description : Computation at lastro.]])

help([[Computation at lastro.]])


-- load modules
load("python-3.6")
load("gsl/2.5")
load("openmpi/3.1.3")
load("fftw/3.3.4")
load("hdf5/1.10.4")
load("grackle/3.0")


-- setup some environement variables
setenv("CLASTRO_PYTHON","/usr/bin/python3")                             -- path to the python executable
setenv("CLASTRO_PYTHON_INSTALL_PATH","/home/revaz/local/python-3.6/" )  -- path to the module install directory
setenv("CLASTRO_DIRECTORY", "/home/revaz/local/python-3.6/clastro" )    -- path to the git repo
